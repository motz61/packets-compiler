#include "parser.hpp"
#include "tokenizer.hpp"

#include <boost/lexical_cast.hpp>
#include <iostream>

#define EXPECT_TOKEN(expectedToken) {\
    auto token = tokens[++i];\
    if(token.type != expectedToken)\
        throw UnexpectedToken(token, {expectedToken});\
    }

#define SKIP_COMMENTS(name) while(name.type == TOKEN_COMMENT)\
    name = tokens[++i]

Parser::Parser() = default;

PacketsFile Parser::Parse(const std::string &file)
{
    Tokenizer tokenizer;
    PacketsFile ret;

    auto tokens = tokenizer.Tokenize(file);

    for(auto i = 0; i < tokens.size(); i++) {
        std::cout << tokens[i].ToString() << std::endl;
    }

    for (auto i = 0; i < tokens.size(); i++)
    {
        auto &token = tokens[i];
        SKIP_COMMENTS(token);
        // The first token must be a keyword
        if (token.type != TOKEN_KEYWORD)
            throw UnexpectedToken(token, {TOKEN_KEYWORD});

        if (token.value == "namespace")
        {
            ParseNamespace(i, tokens, ret);
        } else if(token.value == "target") {
            ParseTarget(i, tokens, ret);
        } else if(token.value == "packet") {
            auto packet = ParsePacket(i, tokens, ret);
            ret.packets.push_back(packet);
            std::cout << ret.packets.size() << std::endl;
        } else if(token.value == "core-directory") {
            ParseCoreDirectory(i, tokens, ret);
        } else {
            throw UnexpectedKeyword(token.value, {"namespace", "target", "packet", "core-directory"});
        }
    }

    return ret;
}

Packet Parser::ParsePacket(int &i, std::vector<Token> &tokens, PacketsFile &output) {
    Packet packet{};
    
    auto nameToken = tokens[++i];
    if(nameToken.type != TOKEN_IDENTIFIER)
        throw UnexpectedToken(nameToken, {TOKEN_IDENTIFIER});
    packet.name = nameToken.value;

    EXPECT_TOKEN(TOKEN_OPENING_BRACKET)

    auto directionToken = tokens[++i];
    if(directionToken.type == TOKEN_DIRECTION_IN) {
        packet.direction = Direction::IN;
    } else if(directionToken.type == TOKEN_DIRECTION_OUT) {
        packet.direction = Direction::OUT;
    } else if(directionToken.type == TOKEN_DIRECTION_IN_OUT) {
        packet.direction = Direction::IN_OUT;
    } else {
        throw UnexpectedToken(directionToken, {TOKEN_DIRECTION_IN, TOKEN_DIRECTION_OUT, TOKEN_DIRECTION_IN_OUT});
    }

    auto headerToken = tokens[++i];
    if(headerToken.type != TOKEN_NUMBER)
        throw UnexpectedToken(headerToken, {TOKEN_NUMBER});
    packet.header = headerToken.value;

    EXPECT_TOKEN(TOKEN_CLOSING_BRACKET)
    EXPECT_TOKEN(TOKEN_OPENING_CURLY_BRACKET);

    // We expect fields now
    auto token = tokens[++i];
    while(token.type != TOKEN_CLOSING_CURLY_BRACKET) {
        if(token.type != TOKEN_KEYWORD && token.type != TOKEN_IDENTIFIER)
            throw UnexpectedToken(token, {TOKEN_KEYWORD, TOKEN_IDENTIFIER});

        if(token.type == TOKEN_KEYWORD && (token.value == "sequence" || token.value == "dynamic")) {
            if(token.value == "sequence") {
                packet.sequence = true;
            }
            if(token.value == "dynamic") {
                packet.dynamic = true;
            }

            EXPECT_TOKEN(TOKEN_END_COMMAND);
            token = tokens[++i];
            continue;
        }
        
        if(token.type == TOKEN_KEYWORD && token.value == "type") {
            // sub type
            auto nameToken = tokens[++i];
            if(nameToken.type != TOKEN_IDENTIFIER)
                throw UnexpectedToken(nameToken, {TOKEN_IDENTIFIER});

            Type type;
            type.name = nameToken.value;

            EXPECT_TOKEN(TOKEN_OPENING_CURLY_BRACKET);
            token = tokens[++i];
            while(token.type != TOKEN_CLOSING_CURLY_BRACKET) {
                auto field = ParseField(i, tokens, packet);
                type.fields.push_back(field);

                token = tokens[++i];
            }

            packet.types[type.name] = type;

            token = tokens[++i];
            continue;
        }

        auto field = ParseField(i, tokens, packet);

        packet.fields.push_back(field);
        token = tokens[++i];
    }

    return packet;
}

Field Parser::ParseField(int &i, std::vector<Token> &tokens, Packet &packet) {
    Field field{};
    auto token = tokens[i];

    std::cout << "parse field " << token.ToString() << std::endl;

    // Check if we have type
    if(token.type == TOKEN_KEYWORD) {
        field.primitive = true;
        if (token.value != "uint8" && token.value != "int8" && token.value != "uint16" && token.value != "int16" &&
            token.value != "uint32" && token.value != "int32" && token.value != "raw" && token.value != "string" && \
            token.value != "float")
            throw UnexpectedKeyword(token.value,
                                    {"uint8", "int8", "uint16", "int16", "uint32", "int32", "raw", "string", "float"});
    } else {
        field.primitive = false;
        if(packet.types.find(token.value) == packet.types.end())
            throw UnexpectedKeyword(token.value, {"defined types"});
    }

    field.type = token.value;

    // if raw or string we have size too
    if(token.value == "string" || token.value == "raw") {
        EXPECT_TOKEN(TOKEN_OPENING_BRACKET);

        auto lengthToken = tokens[++i];
        if(lengthToken.type != TOKEN_NUMBER)
            throw UnexpectedToken(lengthToken, {TOKEN_NUMBER});

        field.length = boost::lexical_cast<unsigned int>(lengthToken.value);

        EXPECT_TOKEN(TOKEN_CLOSING_BRACKET);
    }

    // expect name
    auto nameToken = tokens[++i];
    if(nameToken.type != TOKEN_IDENTIFIER)
        throw UnexpectedToken(nameToken, {TOKEN_IDENTIFIER});

    field.name = nameToken.value;

    // if next token is open square bracket we have an array
    auto nextToken = tokens[++i];
    field.arraySize = -1;
    if(nextToken.type == TOKEN_OPENING_SQUARE_BRACKET) {
        auto lengthToken = tokens[++i];
        if(lengthToken.type != TOKEN_NUMBER)
            throw UnexpectedToken(lengthToken, {TOKEN_NUMBER});

        field.arraySize = boost::lexical_cast<unsigned int>(lengthToken.value);

        EXPECT_TOKEN(TOKEN_CLOSING_SQUARE_BRACKET)
        EXPECT_TOKEN(TOKEN_END_COMMAND)
    }
        // else the next token must be an end command
    else if(nextToken.type != TOKEN_END_COMMAND) {
        throw UnexpectedToken(nextToken, {TOKEN_OPENING_SQUARE_BRACKET, TOKEN_END_COMMAND});
    }

    return field;
}

void Parser::ParseNamespace(int &i, std::vector<Token> &tokens, PacketsFile &output)
{
    auto valueToken = tokens[++i];
    if(valueToken.type != TOKEN_STRING)
        throw UnexpectedToken(valueToken, {TOKEN_STRING});

    output.namespaceValue = valueToken.value;

    EXPECT_TOKEN(TOKEN_END_COMMAND);
}

void Parser::ParseTarget(int &i, std::vector<Token> &tokens, PacketsFile &output)
{
    auto valueToken = tokens[++i];
    if(valueToken.type != TOKEN_STRING)
        throw UnexpectedToken(valueToken, {TOKEN_STRING});

    output.targetValue = valueToken.value;

    EXPECT_TOKEN(TOKEN_END_COMMAND)
}
void Parser::ParseCoreDirectory(int &i, std::vector<Token> &tokens, PacketsFile &output) {
    auto valueToken = tokens[++i];
    if(valueToken.type != TOKEN_STRING)
        throw UnexpectedToken(valueToken, {TOKEN_STRING});

    output.coreDirectory = valueToken.value;
    // Remove trailing slash
    if(output.coreDirectory.substr(output.coreDirectory.size() - 1, 1) == "/") {
        output.coreDirectory = output.coreDirectory.substr(0, output.coreDirectory.size()-1);
    }

    EXPECT_TOKEN(TOKEN_END_COMMAND)
}

UnexpectedToken::UnexpectedToken(Token token, std::vector<TokenType> expected) : _err()
{
    _err << "Unexpected token " << TokenToString(token.type) << " at " << token.lineNo << ":" << token.pos << " expected: ";
    for (auto &token : expected)
    {
        _err << TokenToString(token) << " ";
    }
    _what = _err.str();
}

const char *UnexpectedToken::what() const throw()
{
    return _what.c_str();
}

UnexpectedKeyword::UnexpectedKeyword(std::string keyword, std::vector<std::string> expected) : _err()
{
    _err << "Unexpected keyword " << keyword << " expected: ";
    for (auto &keyword : expected)
    {
        _err << keyword << " ";
    }
    _what = _err.str();
}

const char *UnexpectedKeyword::what() const throw()
{
    return _what.c_str();
}